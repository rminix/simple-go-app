# Echo HTTP Headers JSON Response

This is a simple Go program built using the Echo framework that returns the HTTP headers of an incoming request as a JSON response. The program listens for incoming HTTP requests on port 8080, and when a request is received, it extracts the headers and returns them as a JSON response.
