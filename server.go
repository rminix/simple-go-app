package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
)

func main() {

	app := echo.New()
	app.GET("/", func(c echo.Context) error {
		hostname, err := os.Hostname()
		fmt.Println(c.Request().RemoteAddr)
		if err != nil {
			return err
		}

		// Set custom request header
		c.Request().Header.Set("Machine", hostname)

		// Get the all HTTP headers
		headers := c.Request().Header

		// Convert headers to map[string][]string
		headersMap := make(map[string][]string)
		for key, value := range headers {
			headersMap[key] = value
		}

		// Convert headersMap to JSON string
		headersJSON, err := json.Marshal(headersMap)
		if err != nil {
			return err
		}

		// Set custome response header
		c.Response().Header().Set("Machine", hostname)

		// Return JSON response
		return c.JSONBlob(http.StatusOK, headersJSON)

	})
	app.Logger.Fatal(app.Start(":8080"))
}
